# secure-bash-script

Protect your bash scripts by encrypting them with GnuPG.

## How to use

Suppose that you have sensitive information in a script. Then you can encrypt it using GnuPG and protect it with a password, that you have to enter every time when you execute it (unless you are using the GnuPG agent).

For instance, run:

```
./do-secure-bash-script dummy-passwords.sh
```

then enter twice a password (in my example `test`) and you get the encrypted script

```
dummy-passwords-secured.sh
```

## Known problems

When using remotely sometimes you get the following error message while executing the "secured" script:

```
gpg: public key decryption failed: Invalid IPC response
gpg: decryption failed: No secret key
```

One solution is just to define the following environment variable:

```
export GPG_TTY=$(tty)
```
