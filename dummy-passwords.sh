#! /bin/bash

usage() {
	echo;
	echo "  Usage: $0 {-h,-p1,-p2}";
	echo "   -p1 : get password 1";
	echo "   -p2 : get password 2";
	echo "   -h : help":
	echo;
	exit;
}

[ -z "$1" ] && usage;
[ "$1" = "-p1" ] && echo "123456";
[ "$1" = "-p2" ] && echo "password";
[ "$1" = "-h" ] && usage;

